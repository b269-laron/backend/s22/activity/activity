/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

function register (register_user){
    if (registeredUsers.includes(register_user)){
         alert('Registration failed. Username already exists!')
        } else {
            (registeredUsers.push(register_user));
            alert('Thank you for registering');
}
}

function addFriend (new_friend){
    if (registeredUsers.includes(new_friend)){
        friendsList.push(new_friend)
    alert('You have added ' + new_friend + ' as a friend.')
        } else{
    alert('User not found');
}
}

function displayFriend(){
    if (friendsList.length === 0){
        alert('You have 0 Friend. Add one first.')
    }else{
        console.log(friendsList)
    }
}

function displayNumberOfFriends(){
    if (friendsList.length === 0){
        alert('You have 0 Friend. Add one first.')
    }else{
        alert('You currently have ' + friendsList.length + ' friends.');
    }
}

function deleteFriend(){
        if(friendsList.length >= 1){
            friendsList.splice(0, 1)
        }else{
           alert('You have 0 Friend. Add one first.') 
        }
}
        
